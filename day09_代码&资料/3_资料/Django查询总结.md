```python
# * 1.查询编号为1的图书 [三种方式]
# print(BookInfo.objects.get(id=1))
# print(BookInfo.objects.filter(id__exact=1).first())
# print(BookInfo.objects.filter(id=1).first())

# * 2.查询书名包含雪山的图书
# print(BookInfo.objects.filter(btitle__contains='雪山').all())
# * 3.查询书籍名不为空的图书
# print(BookInfo.objects.filter(btitle__isnull=False).all())
# * 4.查询书籍编号为1,3,5的图书
# print(BookInfo.objects.filter(id__in=[1,3,5]).all())

# * 5.查询编号大于3的图书
# print(BookInfo.objects.filter(id__gt=3))

# * 6.查询编号不等于3的图书
# print(BookInfo.objects.exclude(id=3))
# print(BookInfo.objects.exclude(id__exact=3))

# * 7.查询1980年发表的图书
# print(BookInfo.objects.filter(bpub_date__year="1980").all())

# * 8.查询1980年1月1号之后发表的图书
# print(BookInfo.objects.filter(bpub_date__gt="1980-1-1"))

# * 9.查询阅读量大于等于评论量的图书
# from django.db.models import F
# print(BookInfo.objects.filter(bread__gte=F("bcomment")))

# * 10.查询阅读量大于2倍评论量的图书
# from django.db.models import F
# print(BookInfo.objects.filter(bread__gte=2*F("bcomment")))

# * 11.查询阅读量大于20,并且编号小于3的图书
# print(BookInfo.objects.filter(bread__gt=20,id__lt=3))
# print(BookInfo.objects.filter(bread__gt=20).filter(id__lt=3))

# from django.db.models import Q
# print(BookInfo.objects.filter(Q(bread__gt=20),Q(id__lt=3)))
# print(BookInfo.objects.filter(bread__gt=20).filter(id__lt=3))

# * 12. 查询阅读量大于20，或编号小于3的图书，只能使用Q对象实现
# from django.db.models import Q
# print(BookInfo.objects.filter(Q(bread__gt=20) | Q(id__lt=3)))

# * 13.查询编号不等于3的图书,使用Q对象实现
# from django.db.models import Q
# print(BookInfo.objects.filter(~Q(id=3)))

# * 14.查询所有图书,按照阅读量排序,升序
# print(BookInfo.objects.order_by("bread"))

# * 15.查询所有图书,按照阅读量排序,倒序
# from django.db.models import Q
# print(BookInfo.objects.order_by("-bread"))

# * 16.查询英雄信息,在编号为1的图书中的
# print(HeroInfo.objects.filter(hbook_id=1))

# * 17.查询书籍名称为雪山飞狐的,所有人物信息
# print(HeroInfo.objects.filter(hbook__btitle="雪山飞狐"))

# * 18.查询英雄编号为1,所属的图书信息
# print(BookInfo.objects.filter(heroinfo=1))

# * 19.查询有乔峰的书籍
# print(BookInfo.objects.filter(heroinfo__hname="乔峰"))

# * 20.查询书籍中,英雄包含''郭''的书籍
# print(BookInfo.objects.filter(heroinfo__hname__contains="郭"))

# * 21.查询天龙八部中,所有的英雄信息   和16个查询语句是重复的
# print(HeroInfo.objects.filter(hbook__btitle="天龙八部"))

# * 22.查询阅读量大于30的所有英雄
print(HeroInfo.objects.filter(hbook__bread__gt=30))
```

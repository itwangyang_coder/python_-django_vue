from django.db import models

class Course(models.Model):
    name = models.CharField(max_length=10)

    def __str__(self):
        return self.name

class Student(models.Model):
    name = models.CharField(max_length=10)
    age = models.IntegerField()
    #多对多字典,指定之后, 会自动关联表的主键, 并迁移的时候,会生成中间表
    courses = models.ManyToManyField('Course',related_name="students")

    def __str__(self):
        return self.name

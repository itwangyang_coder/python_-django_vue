from django.shortcuts import render

from users.models import Student,Course

# 添加数据
student1 = Student.objects.create(name="张三",age=13)
student2 = Student.objects.create(name="李四",age=14)
student3 = Student.objects.create(name="王五",age=15)

course1 = Course.objects.create(name="物理")
course2 = Course.objects.create(name="化学")
course3 = Course.objects.create(name="生物")

# 建立关系
student1.courses.add(course1,course2)
student2.courses.add(course2,course3)
student3.courses.add(course1,course2,course3)

# 查询, 张三选了哪些课
student1.courses.all()

# 查询, 生物被哪些学生选过
course3.student_set.all()

# 删除, 生物这门课所有的学生
course = Course.objects.get(name='生物')
students = course.students.all()
course.students.remove(*students)
course.students.all()
from rest_framework.viewsets import ModelViewSet
from meiduo_admin.my_pagination import MyPageNumberPagination
from . import image_serializers
from goods.models import SKUImage,SKU
from rest_framework.generics import ListAPIView
from rest_framework.response import Response
from rest_framework import status
from fdfs_client.client import Fdfs_client
from django.conf import settings

#1,images 管理
class ImageModelViewSet(ModelViewSet):
    pagination_class = MyPageNumberPagination
    serializer_class = image_serializers.ImageSerializers
    queryset = SKUImage.objects.all()

    #1,重写create方法,上传图片
    def create(self, request, *args, **kwargs):

        #1, 获取参数
        dict_data = request.data
        sku = dict_data.get("sku")
        image = dict_data.get("image")

        #2, 校验参数
        if not all([sku,image]):
            return Response(status=status.HTTP_400_BAD_REQUEST)

        #3, 创建fdfs对象,上传图片
        client = Fdfs_client(settings.FDFS_CONFIG)
        result = client.upload_by_buffer(image.read())

        # 判断是否上传成功
        if result["Status"] != "Upload successed.":
            return Response(status=status.HTTP_400_BAD_REQUEST)

        # 获取图片名称
        image_name = result["Remote file_id"]

        #4, 数据入库
        SKUImage.objects.create(sku_id=sku,image=image_name)
        SKU.objects.filter(id=sku,default_image_url='').update(default_image_url=image_name)

        #5, 返回响应
        return Response(status=status.HTTP_201_CREATED)

    #2,重写update方法,修改图片
    def update(self, request, *args, **kwargs):
        #1, 获取参数
        dict_data = request.data
        sku = dict_data.get("sku") #需要修改的sku_id
        image = dict_data.get("image") #传入进来的新图片
        sku_image = self.get_object() #根据pk获取sku_image对象

        #2, 校验参数
        if not all([sku,image,sku_image]):
            return Response(status=status.HTTP_400_BAD_REQUEST)

        #3, 创建fdfs对象,上传图片
        client = Fdfs_client(settings.FDFS_CONFIG)
        result = client.upload_by_buffer(image.read())

        # 判断是否上传成功
        if result["Status"] != "Upload successed.":
            return Response(status=status.HTTP_400_BAD_REQUEST)

        # 获取图片名称
        image_name = result["Remote file_id"]

        #4, 数据入库
        SKUImage.objects.filter(id=sku_image.id).update(sku_id=sku,image=image_name)

        #更新默认图片
        SKU.objects.filter(id=sku,default_image_url=sku_image.image).update(default_image_url=image_name)

        #5, 返回响应
        return Response(status=status.HTTP_201_CREATED)

#2, images sku 信息
class SKUSimpleView(ListAPIView):
    pagination_class = None
    serializer_class = image_serializers.SKUSimpleSerializers
    queryset = SKU.objects.all()

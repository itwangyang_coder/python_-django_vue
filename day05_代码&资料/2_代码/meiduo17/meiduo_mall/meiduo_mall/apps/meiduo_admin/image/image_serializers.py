from rest_framework import serializers
from goods.models import SKUImage,SKU

#1,image 序列化器
class ImageSerializers(serializers.ModelSerializer):
    class Meta:
        model = SKUImage
        fields = "__all__"

#2, image sku 序列化器
class SKUSimpleSerializers(serializers.ModelSerializer):
    class Meta:
        model = SKU
        fields = ["id","name"]
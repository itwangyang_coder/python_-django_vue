from rest_framework.viewsets import ModelViewSet
from meiduo_admin.my_pagination import MyPageNumberPagination
from . import permission_serializers
from django.contrib.auth.models import Permission
from rest_framework.generics import ListAPIView
from django.contrib.contenttypes.models import ContentType

#1, permission 管理
class PermissionModelViewSet(ModelViewSet):
    pagination_class = MyPageNumberPagination
    serializer_class = permission_serializers.PermissionSerializers
    queryset = Permission.objects.all()

#2, permission content_types
class PermissionContentTypesView(ListAPIView):
    pagination_class = None
    serializer_class = permission_serializers.PermissionContentTypesSerializers
    queryset = ContentType.objects.all()
from rest_framework import serializers
from django.contrib.auth.models import Group,Permission

#1, group 序列化器
class GroupSerializers(serializers.ModelSerializer):
    class Meta:
        model = Group
        fields = "__all__"

#2, permission simple
class PermissionSimpleSerializers(serializers.ModelSerializer):
    class Meta:
        model = Permission
        fields = ["id","name"]
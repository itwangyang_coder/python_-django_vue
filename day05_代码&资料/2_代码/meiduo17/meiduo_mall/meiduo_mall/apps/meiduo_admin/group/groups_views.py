from rest_framework.viewsets import ModelViewSet
from meiduo_admin.my_pagination import MyPageNumberPagination
from . import groups_serializers
from django.contrib.auth.models import Group,Permission
from rest_framework.generics import ListAPIView

#1, group 管理
class GroupModelViewSet(ModelViewSet):
    pagination_class = MyPageNumberPagination
    serializer_class = groups_serializers.GroupSerializers
    queryset = Group.objects.all()

#2, permission simple
class PermissionSimpleView(ListAPIView):
    pagination_class = None
    serializer_class = groups_serializers.PermissionSimpleSerializers
    queryset = Permission.objects.all()
from rest_framework import serializers
from orders.models import OrderInfo,OrderGoods
from goods.models import SKU

#1,order序列化器
class OrderSerializers(serializers.ModelSerializer):
    #1,重写user
    user = serializers.StringRelatedField(read_only=True)

    class Meta:
        model = OrderInfo
        fields = "__all__"


#2,order retrieve
#sku
class SKUSerializers(serializers.ModelSerializer):
    class Meta:
        model = SKU
        fields = ["name","default_image_url"]

#ordergoods
class OrderGoodsSerializers(serializers.ModelSerializer):
    #1,重写sku
    sku = SKUSerializers(read_only=True)

    class Meta:
        model = OrderGoods
        fields = ["sku","price","count"]

#orderinfo
class OrderRetrieveSerializers(serializers.ModelSerializer):
    # 1,重写user
    user = serializers.StringRelatedField(read_only=True)

    # 2,重写订单商品属性
    skus = OrderGoodsSerializers(read_only=True,many=True)

    class Meta:
        model = OrderInfo
        fields = "__all__"
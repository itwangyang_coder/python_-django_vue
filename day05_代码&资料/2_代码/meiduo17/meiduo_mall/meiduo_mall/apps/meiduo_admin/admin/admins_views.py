from rest_framework.viewsets import ModelViewSet
from meiduo_admin.my_pagination import MyPageNumberPagination
from . import admins_serializers
from users.models import User
from rest_framework.generics import ListAPIView
from django.contrib.auth.models import Group
from rest_framework.response import Response
from rest_framework import  status

#1, admin管理
class AdminModelViewSet(ModelViewSet):
    pagination_class = MyPageNumberPagination
    serializer_class = admins_serializers.AdminSerializers
    # is_staff=True是管理员;  is_staff=True和is_superuser=True是超级管理员
    queryset = User.objects.filter(is_staff=True).all()

    #1,重写destory方法,禁止自己删自己
    def destroy(self, request, *args, **kwargs):

        #判断删除的是否是自己
        user = self.get_object()
        if user == request.user:
            return Response({"errmsg":"不能自己删自己"},status=status.HTTP_400_BAD_REQUEST)

        return super(AdminModelViewSet, self).destroy(request,*args,**kwargs)

#2, group simple
class GroupSimpleView(ListAPIView):
    pagination_class = None
    serializer_class = admins_serializers.GroupSimpleSerializers
    queryset = Group.objects.all()
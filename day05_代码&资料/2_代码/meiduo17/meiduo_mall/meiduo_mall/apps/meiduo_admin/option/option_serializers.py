from rest_framework import serializers
from goods.models import SpecificationOption,SPUSpecification

#1, option 序列化器
class OptionSerializers(serializers.ModelSerializer):

    #1,重写规格spec,spec_id
    spec = serializers.CharField(read_only=True)
    spec_id = serializers.IntegerField()

    class Meta:
        model = SpecificationOption
        fields = "__all__"

#2, option spec 序列化器
class SpecSimpleSerializers(serializers.ModelSerializer):
    class Meta:
        model = SPUSpecification
        fields = ["id","name"]
from rest_framework.viewsets import ModelViewSet
from meiduo_admin.my_pagination import MyPageNumberPagination
from . import option_serializers
from goods.models import SpecificationOption,SPUSpecification
from rest_framework.generics import ListAPIView

#1, option 管理
class OptionModelViewSet(ModelViewSet):
    pagination_class = MyPageNumberPagination
    serializer_class = option_serializers.OptionSerializers
    queryset = SpecificationOption.objects.all()

#2, option spec 获取
class SpecSimpleView(ListAPIView):
    pagination_class = None
    serializer_class = option_serializers.SpecSimpleSerializers
    queryset = SPUSpecification.objects.all()

    #1,重写get_queryset,修改name的值, 目的:在前端页面显示可读性更好
    def get_queryset(self):
        #1,获取所有的数据
        queryset = SPUSpecification.objects.all()

        #2,拼接name
        for spec in queryset:
            spec.name = "{} - {}".format(spec.spu.name,spec.name)

        #3,返回
        return queryset
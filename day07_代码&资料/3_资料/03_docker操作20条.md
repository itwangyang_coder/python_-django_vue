##### 1,搜索hello-world镜像

-  docker search hello-world

##### 2,下载hello-world镜像

- docker pull hello-world
- docker image pull hello-world

##### 3, 查看所有镜像

- docker images
- docker image ls

##### 4,查看hello-world镜像历史

- docker history hello-world

##### 5,将hello-world:latest镜像, 备份为hello-world:v2.0镜像

- docker tag hello-world:latest hello-world:v2.0

##### 6,删除hello-world:v2.0镜像

- docker rmi hello-world:v2.0

##### 7,清除未被使用的镜像

- docker image prune

##### 8,导出hello-world:latest镜像

- docker save -o hello-world:latest.tar hello-world:latest

##### 9,导入hello-world镜像

- docker load -i hello-world\:latest.tar

##### 10,查看所有容器

- docker ps
- docker ps -a

##### 11,查看所有的容器编号

- docker ps -q

- docker ps -aq

##### 12,守护进程启动nginx1容器

- docker run -d --name nginx1 nginx:latest 

##### 13,启动nginx1容器

- docker start nginx1 

##### 14,停止nginx1容器

- docker stop nginx1
- docker container stop nginx1

##### 15,删除nginx1容器

- docker rm nginx1

##### 16,批量删除正在运行的nginx容器

- docker rm $(docker ps -q) -f

##### 17,创建nginx容器并进入

- docker run -it --name nginx nginx:latest /bin/bash

##### 18,重新进入nginx容器

- docker exec -it nginx /bin/bash

##### 19,通过nginx容器创建nginx:v1.0镜像

- docker commit -m 'haha' -a 'laowang' nginx nginx:v1.0

##### 20,查看nginx镜像,容器全部信息

- docker image inspect nginx:v1.0
- docker container inspect nginx



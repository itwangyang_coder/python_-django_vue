from django.conf.urls import url
from rest_framework_jwt.views import obtain_jwt_token
from meiduo_admin.home import home_views
from meiduo_admin.user import user_views
from rest_framework.routers import DefaultRouter
from meiduo_admin.sku import sku_views

urlpatterns = [
    url(r'^authorizations/$',obtain_jwt_token),
    url(r'^statistical/total_count/$',home_views.UserTotalCountView.as_view()),
    url(r'^statistical/day_increment/$',home_views.UserDayIncrementView.as_view()),
    url(r'^statistical/day_active/$',home_views.UserDayActiveView.as_view()),
    url(r'^statistical/day_orders/$',home_views.UserDayOrdersView.as_view()),
    url(r'^statistical/month_increment/$',home_views.UserMonthIncrementView.as_view()),
    url(r'^statistical/goods_day_views/$',home_views.GoodCategoryDayView.as_view()),
    url(r'^users/$',user_views.UserView.as_view()),

    #1,skus/categories
    url(r'^skus/categories/$',sku_views.SKUCategoryView.as_view()),

    # goods/simple/
    url(r'^goods/simple/$',sku_views.GoodSimpleView.as_view()),

    # goods/3/specs/
    url(r'^goods/(?P<spu_id>\d+)/specs/$',sku_views.GoodSpecsView.as_view()),
]

#1, skus
router = DefaultRouter()
router.register("skus",sku_views.SKUModelViewSet,base_name="skus")
urlpatterns += router.urls